from random import shuffle

suits = ('Hearts', 'Diamond', 'Spades', 'Clubs')
rank = ('Ace', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven',
        'Eight', 'Nine', 'ten', 'Jack', 'Queen', 'King')

values = {'Ace': 11, 'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5, 'Six': 6, 'Seven': 7, 'Eight': 8,
          'Nine': 9, 'Ten': 10, 'Jack': 10, 'Queen': 10, 'King': 10}

playing = True


# class definations : card , chips, hands , deck

# all cards will have a suit and rank property
# let say ace of diamonds: here ace is card rank and diamond is suit
class Card:
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        return self.rank + ' of ' + self.suit


# deck will contain all cards: we have four suits and 13 cards of each suits
class Deck:

    def __init__(self):
        self.deck = []
        for suit in suits:
            # hearts
            for item in rank:
                # here item will be rank like ace , two...
                self.deck.append(Card(suit, item))

    def shuffle(self):
        shuffle(self.deck)

    def deal(self):
        single_card = self.deck.pop()
        return single_card

    def __str__(self):
        deck = ''
        for item in self.deck:
            deck += '\n' + item.__str__()

        return deck


# hands means no of cards  you are holding
class Hand:
    # initially you dont have any cards so your total value is also 0 and no of aces is also 0
    def __init__(self):
        self.cards = []
        self.value = 0
        self.aces = 0

    def add_card(self, card):
        self.cards.append(card)
        # let say rank of card is ace
        # values['Ace'] = 11
        self.value += values[card.rank]
        if card.rank == 'Ace':
            self.aces += 1

    def adjust_for_aces(self):
        # let say you have three cards : 10,10 and ace
        # so your value is greater than 21 : 10 + 10 +11 = 31
        # but you can use ace as 1 in this case to get 21
        while self.value > 21 and self.aces:
            self.value -= 10
            self.aces -= 1


class Chips:
    # initially we are supposing that we have 100rs or coins
    def __init__(self):
        self.total = 100
        self.bet = 0

    # for example, you have placed 50 for bet and you loose. your initial total no of chips were 100
    # now you have lost 50 so total left will be total - (bet you have placed)
    # so it will be 100 - 50 = 50
    def loose_bet(self):
        self.total -= self.bet

    # for example, you have placed 50 for bet and you won. your initial total no of chips were 100
    # now you have won 50 so total left will be total + (bet you have placed)
    # so it will be 100 + 50 = 150
    def win_bet(self):
        self.total += self.bet


def place_bet(chips):
    while True:
        try:
            chips.bet = int(input('How many chips you want to bet?'))
        except Exception as e:
            print(e)
        else:
            break


def hit(deck, hand):
    hand.add_card(deck.deal())
    hand.adjust_for_aces()


def show_some(player, dealer):
    print('\n Dealers Hand')
    print('<Cards Hidden>')
    print(f'one card of dealer: {dealer.cards[1]}')
    print(f'players card are : {player.cards[0]} and {player.cards[1]}')


def show_all(player, dealer):
    print(f'Dealer hand is:,', *dealer.cards, sep='\n')
    print(f'Dealer Value: {dealer.value}')
    print(f'Player hand is:,', *player.cards, sep='\n')
    print(f'Player Value: {player.value}')


def hit_or_stand(deck, hand):
    global playing
    while True:
        x = input('Would you like to pick more cards or stand? Enter h or s')
        if x[0].lower() == 'h':
            hit(deck, hand)
        if x[0].lower() == 's':
            print('player is standing now and dealer will player')
            playing = False
        else:
            print('Wrong Value.Please try again')
            continue
        break


def player_bursts(chips):
    print('Player Bursts!!')
    chips.loose_bet()


def player_wins(chips):
    print('Player won!! :)')
    chips.win_bet()


def dealer_bursts(chips):
    print('Player Bursts!!')
    chips.win_bet()


def dealer_wins(chips):
    print('Player won!! :)')
    chips.loose_bet()


def tie():
    print('Player and Dealer tie!!')
